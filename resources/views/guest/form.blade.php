<!DOCTYPE html>
<html lang="en">
<head>
  <title>Kolega Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="container">

  <div class="form-group">
    <div class="col-sm-12">
      <h2>Guest Book</h2>
    </div>
  </div>

  <form class="form-horizontal" action="/submit" method="POST">

    {{ csrf_field() }}

    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter Your Email" name="email">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="first_name">First Name:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="first_name" placeholder="Enter Your First Name" name="first_name">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="last_name">Last Name:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="last_name" placeholder="Enter Your Last Name" name="last_name">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="job_title">Job Title:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="job_title" placeholder="Enter Your Job or Position" name="job_title">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="company">Company:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="company" placeholder="Enter Your Company" name="company">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="phone">Phone:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="phone" placeholder="Enter Your Phone Number" name="phone">
      </div>
    </div>

    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Reset</button>
      </div>
    </div>

  </form>
</div>

</body>
</html>
