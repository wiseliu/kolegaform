<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guest;
use Validator;
use Response;

class GuestController extends Controller
{
    public function index()
    {
        return view('guest.form');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'alpha|max:255',
            'email' => 'required|email|max:255',
            'job_title' => 'string|max:255',
            'company' => 'required|string|max:255',
            'phone' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('/')->withErrors($validator)->withInput();
        }
        $guest = Guest::create($request->toArray());
        return view('guest.success', ['guest' => $guest]);
    }

    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=guests.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
    
        $guests = Guest::all();
        $columns = array('ID', 'First Name', 'Last Name', 'Email', 'Job Title', 'Company', 'Phone',);
    
        $callback = function () use ($guests, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
    
            foreach ($guests as $guest) {
                fputcsv($file, array($guest->id, $guest->first_name, $guest->last_name, $guest->email, $guest->job_title, $guest->company, $guest->phone,));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
}
